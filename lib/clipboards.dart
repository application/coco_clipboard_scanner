class Clipboards {
  final String content;
  final int createdAt;
  final int cType;
  final int index;

  bool isCopied;

  Clipboards(this.index, this.content, this.createdAt, this.cType, this.isCopied);

  factory Clipboards.fromJson(Map<String, dynamic> json) {
    return Clipboards(json['index'], json['content']??'已被清理的数据', json['created_at']??0, json['type']??0, false);
  }
}