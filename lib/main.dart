import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'clipboards.dart';
import 'FadeAnimation.dart';
import 'package:badges/badges.dart';
import 'package:sprintf/sprintf.dart';

void main() {
  runApp(const MaterialApp(
    title: 'coco',
    home: HomePage(),
    debugShowCheckedModeBanner: false,
  ));
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var textImage = Image.asset("assets/images/text.png");
  var imgImage = Image.asset("assets/images/image.png");
  var fileImage = Image.asset("assets/images/file.png");

  List<dynamic> clipboardList = [];

  Future<void> loadClipboardList() async {
    var httpClient = HttpClient();
    try {
      var uri = Uri.https('central.xuthus.cc', '/api/clipboard/list');
      var request = await httpClient.getUrl(uri);
      var response = await request.close();
      if (response.statusCode == HttpStatus.ok) {
        var content = await response.transform(const Utf8Decoder()).join();
        final data = await json.decode(content);
        setState(() {
          if (data['err_code'] == 0) {
            var tList = data['data']['data'];
            if (tList is List) {
              var idx = 1;
              for (var element in tList) {
                if (element is Map) {
                  element['index'] = idx;
                  idx++;
                }
              }
            }
            clipboardList = tList.map((e) => Clipboards.fromJson(e)).toList();
          } else {
            // 不能崩坏了
            List<Clipboards> list = [
              Clipboards(0, data['err_msg'], 0, 0, false),
            ];
            clipboardList = list;
          }
        });
      } else {
        setState(() {
          List<Clipboards> list = [
            Clipboards(0, "网络请求失败", 0, 0, false),
          ];
          clipboardList = list;
        });
      }
    } on HttpException {
      setState(() {
        List<Clipboards> list = [
          Clipboards(0, "网络请求失败", 0, 0, false),
        ];
        clipboardList = list;
      });
    }
  }

  Future<void> uploadClipboardContent() async {
    ClipboardData? clipboardContent = await Clipboard.getData(
        Clipboard.kTextPlain);

    var url = Uri.parse('https://central.xuthus.cc/api/clipboard/add');
    try {
      final payload = jsonEncode({
        'text': clipboardContent?.text,
        'type': 0,
      });
      Map<String, String> headers = {'Content-Type': 'application/json'};
      var response = await http.post(url, body: payload, headers: headers);
      if (response.statusCode == HttpStatus.ok) {
        final data = jsonDecode(utf8.decode(response.bodyBytes));
        setState(() {
          if (data['err_code'] == 0) {
            List<Clipboards> list = [
              Clipboards(0, '上传成功,点击同步按钮进行数据同步', 0, 0, false),
            ];
            clipboardList = list;
          } else {
            // 不能崩坏了
            List<Clipboards> list = [
              Clipboards(0, data['err_msg'], 0, 0, false),
            ];
            clipboardList = list;
          }
        });
      } else {
        setState(() {
          List<Clipboards> list = [
            Clipboards(0, "网络请求失败", 0, 0, false),
          ];
          clipboardList = list;
        });
      }
    } on HttpException {
      setState(() {
        List<Clipboards> list = [
          Clipboards(0, "网络请求失败", 0, 0, false),
        ];
        clipboardList = list;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    loadClipboardList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color(0xFF26C6DA),
        elevation: 0,
        leadingWidth: 20,
        actions: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
            child: IconButton(
                iconSize: 30,
                onPressed: () {
                  uploadClipboardContent();
                },
                icon: const Icon(
                  Icons.upload_sharp,
                  color: Colors.yellow,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
                iconSize: 30,
                onPressed: () {
                  loadClipboardList();
                },
                icon: Icon(
                  Icons.sync_sharp,
                  color: Colors.red.shade400,
                )),
          ),
        ],
        title: const Text('coco'),
      ),
      body: ListView.builder(
          padding: const EdgeInsets.all(20),
          itemCount: clipboardList.length,
          itemBuilder: (context, index) {
            return FadeAnimation((1.0 + index) / 4,
                dataListComponent(clipboard: clipboardList[index]));
          }),
    );
  }

  unixTimeToString(int utx) {
    var dt = DateTime.fromMillisecondsSinceEpoch(utx * 1000).toLocal();
    return sprintf("%d-%d-%d %d:%d:%d",
        [dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second]);
  }

  getClipboardType(int typ) {
    switch (typ) {
      case 0:
        return "类型: 文字";
      case 1:
        return "类型: 图片";
      case 2:
        return "类型: 文件";
      default:
        return "类型: 未知";
    }
  }

  dataListComponent({required Clipboards clipboard}) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(bottom: 15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 0,
              blurRadius: 2,
              offset: const Offset(0, 1),
            ),
          ]),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Row(children: [
                  SizedBox(
                    width: 30,
                    height: 30,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: clipboard.cType == 0
                          ? textImage
                          : clipboard.cType == 1
                          ? imgImage
                          : fileImage,
                    ),
                  ),
                  const SizedBox(width: 10),
                  Flexible(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(clipboard.content,
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500)),
                          // const SizedBox(
                          //   height: 5,
                          // ),
                          // Text(clipboard.content,
                          //     style: TextStyle(color: Colors.grey[500])),
                        ]),
                  )
                ]),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    Clipboard.setData(ClipboardData(text: clipboard.content));
                    clipboard.isCopied = !clipboard.isCopied;
                  });
                },
                child: AnimatedContainer(
                    height: 35,
                    padding: const EdgeInsets.all(5),
                    duration: const Duration(milliseconds: 300),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        border: Border.all(
                          color: clipboard.isCopied
                              ? Colors.red.shade100
                              : Colors.grey.shade300,
                        )),
                    child: Center(
                        child: clipboard.isCopied
                            ? const Icon(
                          Icons.content_copy,
                          color: Colors.red,
                        )
                            : Icon(
                          Icons.content_copy_outlined,
                          color: Colors.grey.shade600,
                        ))),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Badge(
                    toAnimate: false,
                    shape: BadgeShape.square,
                    badgeColor: const Color(0xD78E8D8D),
                    borderRadius: BorderRadius.circular(15),
                    badgeContent: Text(unixTimeToString(clipboard.createdAt),
                        style:
                        const TextStyle(color: Colors.white, fontSize: 12)),
                  ),
                  const SizedBox(width: 10),
                  Badge(
                    toAnimate: false,
                    shape: BadgeShape.square,
                    badgeColor: Colors.deepPurple,
                    borderRadius: BorderRadius.circular(15),
                    badgeContent: Text(getClipboardType(clipboard.cType),
                        style:
                        const TextStyle(color: Colors.white, fontSize: 12)),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
